package com.example.nizomjon.enjoyweather.views.main.fragments.fragmentWeather;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nizomjon.enjoyweather.R;
import com.example.nizomjon.enjoyweather.base.BaseFragment;
import com.example.nizomjon.enjoyweather.base.components.DaggerActivityComponent;
import com.example.nizomjon.enjoyweather.base.module.PresenterModule;
import com.example.nizomjon.enjoyweather.base.module.UtilsModule;
import com.example.nizomjon.enjoyweather.databinding.FragmentWeatherBinding;
import com.example.nizomjon.enjoyweather.model.CurrentWeatherSk;
import com.example.nizomjon.enjoyweather.model.test.Forecast;
import com.example.nizomjon.enjoyweather.model.test.Precipitation;
import com.example.nizomjon.enjoyweather.model.test.Response;
import com.example.nizomjon.enjoyweather.model.test.Temperature;
import com.example.nizomjon.enjoyweather.utils.AppUtils;
import com.example.nizomjon.enjoyweather.utils.DialogsUtil;
import com.example.nizomjon.enjoyweather.utils.Settings;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.subjects.PublishSubject;

/**
 * Created by Nizomjon on 22/02/2017.
 */

public class WeatherFragment extends BaseFragment implements WeatherView {


    public static final String TAG_TEMPERATURE = "Temperature";
    public static final String TAG_HUMIDITY = "Humidity";

    @Inject
    Settings settings;
    @Inject
    DialogsUtil dialogsUtil;
    @Inject
    AppUtils appUtils;
    @Inject
    WeatherPresenter weatherPresenter;

    @BindView(R.id.current_weather_container)
    FrameLayout layout;
    @BindView(R.id.precipitation_viewgroup)
    LinearLayout precipitationLayout;
    @BindView(R.id.current_weather_details)
    LinearLayout currentWeatherDetails;
    @BindView(R.id.weather_icon)
    ImageView weatherIcon;
    @BindView(R.id.weather_status)
    TextView weatherStatus;


    LineChart mTempChart, mHumidityChart, forecast10daysChart;
    LinearLayout timeTempLayout, timeHumidLayout, timeForecast10daysLayout;
    LinearLayout valuePrecipLayout, valueHumidLayout;
    TextView tab1, tab2;

    FragmentWeatherBinding binding;

    // Resources
    @BindColor(R.color.white)
    int whiteColor;
    @BindColor(R.color.colorPrimary)
    int primaryColor;
    @BindColor(R.color.colorPrimaryDark)
    int primaryDark;
    @BindDrawable(R.drawable.tab_background)
    Drawable drawable;
    int extraHeight;
    private int counter = 0;
    int countTime = 0;
    private int date = 0;
    private boolean isShown = false;
    private boolean isTomorrow, isAfterTomorrow;
    PublishSubject<HashMap<Integer, String>> releasedTimePublisher;
    HashMap<Integer, String> stringHashMap;


    public static WeatherFragment newInstance(int height) {
        WeatherFragment myFragment = new WeatherFragment();

        Bundle args = new Bundle();
        args.putInt("height", height);
        myFragment.setArguments(args);

        return myFragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(getActivity()))
                .presenterModule(new PresenterModule(getActivity()))
                .build()
                .inject(this);
        weatherPresenter.attachView(this);
        extraHeight = getArguments().getInt("height", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_weather, container, false);
        ButterKnife.bind(this, binding.getRoot());
        releasedTimePublisher = PublishSubject.create();
        stringHashMap = new HashMap<>();
        inflateChartLayout();
        return binding.getRoot();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height - extraHeight - 16));
        initGraph();
        initForecast10daysGraph();
        // TODO make latitude and longitude based on location
        weatherPresenter.sync(36.9910, 127.9259);
        weatherPresenter.forecast3hours(36.9910, 127.9259);
        weatherPresenter.forecast6days(36.9910, 127.9259);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_weather;
    }


    private void inflateChartLayout() {
        View temperatureDetailView = getActivity().getLayoutInflater().inflate(R.layout.chart_layout, null);
        View forecast10daysDetailView = getActivity().getLayoutInflater().inflate(R.layout.chart_layout, null);
        LinearLayout tabViews = new LinearLayout(getActivity());
        tabViews.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        tabViews.setOrientation(LinearLayout.HORIZONTAL);
        tab2 = makeTextView(getActivity(), "Humidity");
        tab1 = makeTextView(getActivity(), "Temperature");
        tabViews.addView(tab1);
        tabViews.addView(tab2);
        TextView forecast3hourReleasedTime = makeTextView(getActivity(), "");
        currentWeatherDetails.addView(makeTitleContainer("3 hour forecast", forecast3hourReleasedTime));
        currentWeatherDetails.addView(tabViews);
        currentWeatherDetails.addView(temperatureDetailView);
        currentWeatherDetails.addView(makeLine());
        timeTempLayout = (LinearLayout) temperatureDetailView.findViewById(R.id.time_temperature);
        valuePrecipLayout = (LinearLayout) temperatureDetailView.findViewById(R.id.value_precipitation);
        mTempChart = (LineChart) temperatureDetailView.findViewById(R.id.lineChart);
        TextView forecast10dayReleasedTime = makeTextView(getActivity(), "");
        currentWeatherDetails.addView(makeTitleContainer("10 day forecast", forecast10dayReleasedTime));
        releasedTimePublisher.subscribe(s -> {
            for (Integer i : s.keySet()) {
                if (i == 1) {
                    forecast3hourReleasedTime.setText("Released " + s.get(1));
                } else if (i == 2) {
                    forecast10dayReleasedTime.setText("Released " + s.get(2));
                }
            }
        });
        timeForecast10daysLayout = (LinearLayout) forecast10daysDetailView.findViewById(R.id.time_temperature);
        timeForecast10daysLayout.setPadding(20, 10, 20, 10);
        forecast10daysChart = (LineChart) forecast10daysDetailView.findViewById(R.id.lineChart);
        currentWeatherDetails.addView(forecast10daysDetailView);
        currentWeatherDetails.addView(makeLine());
    }

    @Override
    public void fillData(CurrentWeatherSk currentWeatherSk) {
        binding.setWeather(currentWeatherSk.getSkWeather());
        weatherIcon.setImageDrawable(getContext().getResources().getDrawable(appUtils.getDayIcon_currentWeather(appUtils.getDayCode_currentWeather(currentWeatherSk.getSkWeather().getWeather().getHourlSk().get(0).getSkySk().getCode()))));
        weatherStatus.setText(currentWeatherSk.getSkWeather().getWeather().getHourlSk().get(0).getSkySk().getName());
        provideViews(currentWeatherSk.getForecast());
    }

    @Override
    public void fillGraph(Response response) {

        List<Entry> entries = new ArrayList<Entry>();
        List<Float> temperatures = response.getWeather().getForecast3days().get(0).getFcst3hours().getTemperatures();
        stringHashMap.put(1, appUtils.formatingDate("hh:mm", response.getWeather().getForecast3days().get(0).getTimeRelease()));
        releasedTimePublisher.onNext(stringHashMap);
        stringHashMap.put(2, "12:00");
        releasedTimePublisher.onNext(stringHashMap);
        releasedTimePublisher.onCompleted();

        for (int i = 0; i < temperatures.size(); i++) {
            entries.add(new Entry(i, temperatures.get(i)));
            fillViews(response, i);
        }

        LineDataSet dataSet = new LineDataSet(entries, "Label");
        dataSet.setValueTextSize(18);
        dataSet.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
        dataSet.setDrawCircles(false);
        dataSet.setValueTextColor(Color.WHITE);
        dataSet.setLineWidth(1.75f);
        dataSet.setCircleRadius(5f);
        dataSet.setCircleHoleRadius(2.5f);
        dataSet.setColor(Color.WHITE);
        dataSet.setCircleColor(Color.WHITE);
        dataSet.setHighLightColor(Color.WHITE);
        LineData lineData = new LineData(dataSet);
        mTempChart.setData(lineData);
        mTempChart.invalidate();

    }

    @Override
    public void fillForecast10days(Response response) {
        List<Entry> entries = new ArrayList<>();
        List<Entry> entriesTmin = new ArrayList<>();
        List<Temperature> temperatures = response.getWeather().getForecast6days().get(0).getTemperatures();

        for (int i = 0; i < temperatures.size(); i++) {
            entries.add(new Entry(i, Float.parseFloat(temperatures.get(i).gettMax())));
            entriesTmin.add(new Entry(i, Float.parseFloat(temperatures.get(i).gettMin())));
            provideForecast10daysViews(response, i);
        }
        LineDataSet dataSet = new LineDataSet(entries, "Label");
        dataSet.setValueTextSize(12);
        dataSet.setMode(LineDataSet.Mode.LINEAR);
        dataSet.setDrawValues(true);
        dataSet.setValueTextColor(Color.RED);
        dataSet.setLineWidth(1.75f);
        dataSet.setCircleRadius(3f);
        dataSet.setCircleHoleRadius(2f);
        dataSet.setColor(Color.RED);
        dataSet.setCircleColor(Color.RED);
        dataSet.setCircleColorHole(Color.RED);
        dataSet.setHighLightColor(Color.RED);
        LineDataSet dataSet1 = new LineDataSet(entriesTmin, "Tmin");
        dataSet1.setValueTextSize(12);
        dataSet1.setMode(LineDataSet.Mode.LINEAR);
        dataSet1.setDrawCircles(true);
        dataSet1.setValueTextColor(Color.WHITE);
        dataSet1.setLineWidth(1.75f);
        dataSet1.setCircleRadius(3f);
        dataSet1.setCircleHoleRadius(2f);
        dataSet1.setColor(Color.WHITE);
        dataSet1.setCircleColorHole(Color.WHITE);
        dataSet1.setCircleColor(Color.WHITE);
        dataSet1.setHighLightColor(Color.WHITE);
        List<ILineDataSet> lineDataSets = new ArrayList<>();
        lineDataSets.add(dataSet);
        lineDataSets.add(dataSet1);
        LineData lineData = new LineData(lineDataSets);
        forecast10daysChart.setData(lineData);
        forecast10daysChart.invalidate();
    }

    private void provideForecast10daysViews(Response response, int i) {
        String codeDay = response.getWeather().getForecast6days().get(0).getSkies().get(i).getSkyAm().getCode();
        String codeNight = response.getWeather().getForecast6days().get(0).getSkies().get(i).getSkPm().getCode();
        LinearLayout timeLayout = new LinearLayout(getActivity());
        timeLayout.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        timeLayout.setOrientation(LinearLayout.VERTICAL);
        timeLayout.setPadding(20, 0, 20, 0);
        timeLayout.setGravity(Gravity.CENTER);
        TextView weekOfName = new TextView(getActivity());
        weekOfName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        weekOfName.setGravity(Gravity.CENTER);
        weekOfName.setMaxLines(1);
        weekOfName.setPadding(10, 0, 10, 0);
        weekOfName.setTextSize(12f);
        weekOfName.setTextColor(whiteColor);
        weekOfName.setText(appUtils.weekOfName(response.getWeather().getForecast6days().get(0).getTimeRelease(), i));
        timeLayout.addView(weekOfName);
        TextView weekOfDay = new TextView(getActivity());
        weekOfDay.setLayoutParams(weekOfName.getLayoutParams());
        weekOfDay.setGravity(Gravity.CENTER);
        weekOfDay.setMaxLines(1);
        weekOfDay.setPadding(10, 0, 10, 0);
        weekOfDay.setTextSize(12f);
        weekOfDay.setTextColor(whiteColor);
        weekOfDay.setText("(" + "0" + i + "/" + "0" + (i + 1) + ")");
        timeLayout.addView(weekOfDay);
        LinearLayout iconsContainer = new LinearLayout(getActivity());
        iconsContainer.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        iconsContainer.setOrientation(LinearLayout.HORIZONTAL);
        ImageView iconDay = new ImageView(getActivity());
        iconDay.setLayoutParams(new LinearLayout.LayoutParams(50, 50));
        iconDay.setImageDrawable(getContext().getResources().getDrawable(appUtils.getDayIcon_currentWeather(appUtils.getDayCode_currentWeather(codeDay))));
        ImageView iconNight = new ImageView(getActivity());
        iconNight.setLayoutParams(new LinearLayout.LayoutParams(50, 50));
        iconNight.setImageDrawable(getContext().getResources().getDrawable(appUtils.getNightIcon_currentWeather(appUtils.getNightCode_currentWeather(codeNight))));
        iconsContainer.addView(iconDay);
        iconsContainer.addView(iconNight);
        timeLayout.addView(iconsContainer);
        timeForecast10daysLayout.addView(timeLayout);
    }

    private void fillViews(Response response, int i) {
        TextView timeT = new TextView(getActivity());
        timeT.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        timeT.setGravity(Gravity.CENTER);
        timeT.setPadding(20, 20, 20, 20);
        timeT.setTextColor(whiteColor);
        String time = calculateDate();
//        if (isTomorrow || isAfterTomorrow) {
//            LinearLayout timeLayout = new LinearLayout(getActivity());
//            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
//            layoutParams.gravity = Gravity.CENTER;
//            timeLayout.setLayoutParams(layoutParams);
//            timeLayout.setOrientation(LinearLayout.VERTICAL);
//            timeLayout.setPadding(20, 20, 20, 20);
//            timeLayout.setGravity(Gravity.CENTER);
//            TextView tomorrowT = new TextView(getActivity());
//            tomorrowT.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//            tomorrowT.setGravity(Gravity.CENTER);
//            tomorrowT.setMaxLines(1);
//            tomorrowT.setText(isTomorrow ? "T" : "NT");
//            tomorrowT.setTextColor(whiteColor);
//            timeT.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//            timeT.setText(time);
//            timeLayout.addView(tomorrowT);
//            timeLayout.addView(timeT);
//            timeTempLayout.addView(timeLayout);
//        } else {
        timeT.setText(time);
        timeTempLayout.addView(timeT);
//        }

        Precipitation precipitation = response.getWeather().getForecast3days().get(0).getFcst3hours().getPrecipitations().get(i);
        TextView prepT = new TextView(getActivity());
        prepT.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        prepT.setGravity(Gravity.CENTER);
        prepT.setPadding(20, 0, 20, 0);
        String prepStr = String.valueOf((int) precipitation.getSinceOntime() + "%");
        prepT.setText(prepStr);
        prepT.setTextColor(whiteColor);
        valuePrecipLayout.addView(prepT);
    }


    private String calculateDate() {
        int extraCount = 3;

        if (!isShown) {
            SimpleDateFormat sdf = new SimpleDateFormat("kk:mm");
            String[] str = sdf.format(new Date()).split(":");

            String hour = str[0]; // 13
            int a = Integer.parseInt(hour) % 3; // 1
            if (a != 0) {
                int b = (Integer.parseInt(hour) - a); //12
                date = b;//12
            } else {
                date = a;
            }
            isShown = true;
        }
        date = date + extraCount; // 15
        if (date == 24) {
            date = 0;
        }
        if (date == 0) {
            countTime++;
            if (countTime == 1) {
                isTomorrow = true;
            } else if (countTime == 2) {
                isAfterTomorrow = true;
            }
            return "00:00";
        }
        isTomorrow = false;
        isAfterTomorrow = false;
        if (date < 10) {
            return "0" + String.valueOf(date) + ":" + "00";
        }
        return String.valueOf((date) + ":" + "00");
    }


    private void provideViews(Forecast forecast) {
        if (forecast != null && forecast.getPrecipitations() != null) {

            LinearLayout timeLayout = new LinearLayout(getActivity());
            timeLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
            timeLayout.setGravity(Gravity.BOTTOM);
            timeLayout.setOrientation(LinearLayout.HORIZONTAL);
            timeLayout.setBackgroundColor(primaryDark);
            precipitationLayout.addView(timeLayout);
            LinearLayout iconLayout = new LinearLayout(getActivity());
            iconLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            iconLayout.setBackgroundColor(primaryDark);
            iconLayout.setOrientation(LinearLayout.HORIZONTAL);
            precipitationLayout.addView(iconLayout);
            LinearLayout precipitationValueLayout = new LinearLayout(getActivity());
            precipitationValueLayout.setLayoutParams(iconLayout.getLayoutParams());
            precipitationValueLayout.setOrientation(LinearLayout.HORIZONTAL);
            precipitationValueLayout.setBackgroundColor(primaryDark);
            precipitationLayout.addView(precipitationValueLayout);
            precipitationLayout.addView(makeLine());

            int size = forecast.getPrecipitations().size();
            for (int i = 0; i < size; i++) {
                counter++;
                Precipitation precipitation = forecast.getPrecipitations().get(i);
                String code = forecast.getSkies().get(i).getCode();
                TextView timeT = new TextView(getActivity());
                timeT.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
                timeT.setGravity(Gravity.CENTER);
                String time = calculateDate();
                timeT.setText(time);
                timeT.setTextColor(whiteColor);
                timeLayout.addView(timeT);

                ImageView iconDay = new ImageView(getActivity());
                iconDay.setLayoutParams(new LinearLayout.LayoutParams(0, 120, 1f));
                if (!code.isEmpty()) {
                    iconDay.setImageDrawable(getContext().getResources().getDrawable(appUtils.getDayIcon_currentWeather(appUtils.getDayCode_currentWeather(code))));
                }
                iconLayout.addView(iconDay);
                TextView precipitationT = new TextView(getActivity());
                precipitationT.setLayoutParams(timeT.getLayoutParams());
                precipitationT.setGravity(Gravity.LEFT | Gravity.CENTER);
                precipitationT.setPadding(10, 0, 10, 0);
                precipitationT.setTextColor(whiteColor);
                if (!String.valueOf(precipitation.getType()).isEmpty()) {
                    precipitationT.setText(String.valueOf(precipitation.getType()));
                    Drawable img = getContext().getResources().getDrawable(R.drawable.umbrella);
                    img.setBounds(-20, 0, 0, 0);
                    precipitationT.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                }
                precipitationValueLayout.addView(precipitationT);
            }
        }
    }

    private void initGraph() {
        mTempChart.getDescription().setEnabled(false);
        mTempChart.getAxisLeft().setDrawGridLines(false);
        mTempChart.getAxisRight().setDrawGridLines(false);
        mTempChart.getXAxis().setDrawGridLines(false);
        mTempChart.setViewPortOffsets(0, 20, 0, 20);
        mTempChart.setDrawGridBackground(false);
        mTempChart.getLegend().setEnabled(false);
        mTempChart.setTouchEnabled(false);
        mTempChart.getAxisLeft().setEnabled(false);
        mTempChart.getAxisLeft().setSpaceTop(40);
        mTempChart.getAxisLeft().setSpaceBottom(40);
        mTempChart.getAxisRight().setEnabled(false);
        mTempChart.getXAxis().setEnabled(false);
        XAxis xAxis = mTempChart.getXAxis();
        xAxis.setDrawLabels(false);
        xAxis.setDrawGridLines(false);
        xAxis.setSpaceMin(0.5f);
        xAxis.setSpaceMax(0.5f);
        YAxis yAxis = mTempChart.getAxisRight();
        yAxis.setDrawGridLines(false);
        yAxis.setDrawLabels(false);
        mTempChart.animateX(2000);
    }

    private void initForecast10daysGraph() {
        forecast10daysChart.getDescription().setEnabled(false);
        forecast10daysChart.getAxisLeft().setDrawGridLines(false);
        forecast10daysChart.getAxisRight().setDrawGridLines(false);
        forecast10daysChart.getXAxis().setDrawGridLines(false);
//        forecast10daysChart.setVisibleXRange(0,5);
        forecast10daysChart.setViewPortOffsets(0, 20, 0, 20);
        forecast10daysChart.setDrawGridBackground(false);
        forecast10daysChart.getLegend().setEnabled(false);
        forecast10daysChart.setTouchEnabled(false);
        forecast10daysChart.getAxisLeft().setEnabled(false);
        forecast10daysChart.getAxisLeft().setSpaceTop(40);
        forecast10daysChart.getAxisLeft().setSpaceBottom(40);
        forecast10daysChart.getAxisRight().setEnabled(false);
        forecast10daysChart.getXAxis().setEnabled(false);
        XAxis xAxis = forecast10daysChart.getXAxis();
        xAxis.setDrawLabels(false);
        xAxis.setDrawGridLines(false);
        xAxis.setSpaceMin(0.5f);
        xAxis.setSpaceMax(0.5f);
        YAxis yAxis = forecast10daysChart.getAxisRight();
        yAxis.setDrawGridLines(false);
        yAxis.setDrawLabels(false);
        forecast10daysChart.animateX(2000);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void setError(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    private TextView makeTextView(Activity activity, String text) {
        TextView textView = new TextView(activity);
        textView.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(whiteColor);
        textView.setTextSize(18f);
        textView.setPadding(20, 0, 20, 0);
        textView.setSelected(true);
        textView.setBackground(drawable);
        textView.setText(text);
        return textView;
    }

    private LinearLayout makeTitleContainer(String leftT, TextView rightTextView) {
        LinearLayout layout = new LinearLayout(getActivity());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 20, 0, 20);
        layout.setLayoutParams(layoutParams);
        layout.setOrientation(LinearLayout.HORIZONTAL);
        layout.setBackgroundColor(primaryDark);
        TextView leftText = makeTextView(getActivity(), leftT);
        leftText.setPadding(20, 0, 20, 0);
        leftText.setTextSize(12f);
        leftText.setGravity(Gravity.LEFT);
        rightTextView.setTextSize(12f);
        rightTextView.setGravity(Gravity.RIGHT);
        layout.addView(leftText, 0);
        layout.addView(rightTextView, 1);
        return layout;
    }

    private View makeLine() {
        View view = new View(getActivity());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1);
        layoutParams.setMargins(0, 30, 0, 30);
        view.setLayoutParams(layoutParams);
        view.setBackgroundColor(whiteColor);
        return view;
    }


}
