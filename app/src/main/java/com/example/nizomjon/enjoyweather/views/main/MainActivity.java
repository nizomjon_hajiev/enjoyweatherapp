package com.example.nizomjon.enjoyweather.views.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.util.TypedValue;
import android.view.ViewTreeObserver;

import com.example.nizomjon.enjoyweather.R;
import com.example.nizomjon.enjoyweather.adapter.ViewPagerAdapter;
import com.example.nizomjon.enjoyweather.base.BaseActivity;
import com.example.nizomjon.enjoyweather.base.components.DaggerActivityComponent;
import com.example.nizomjon.enjoyweather.base.module.PresenterModule;
import com.example.nizomjon.enjoyweather.base.module.UtilsModule;
import com.example.nizomjon.enjoyweather.event.MainEvent;
import com.example.nizomjon.enjoyweather.views.main.fragments.fragmentPi.PiFragment;
import com.example.nizomjon.enjoyweather.views.main.fragments.fragmentPi.PiPresenter;
import com.example.nizomjon.enjoyweather.views.main.fragments.fragmentWeather.WeatherFragment;
import com.example.nizomjon.enjoyweather.widgets.CustomViewPager;
import com.example.nizomjon.enjoyweather.widgets.RxBus;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by Nizomjon on 22/02/2017.
 */

public class MainActivity extends BaseActivity implements TabLayout.OnTabSelectedListener {

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    public CustomViewPager viewPager;
    @Inject
    PiPresenter piPresenter;
    int height, actionBarHeight;

    private RxBus _rxBus;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(this))
                .presenterModule(new PresenterModule(this))
                .build()
                .inject(this);
        setTitle("Enjoy WeatherPi");
        tabLayout.addOnTabSelectedListener(this);
        TypedValue tv = new TypedValue();
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }
        ViewTreeObserver viewTreeObserver = tabLayout.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    tabLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    height = tabLayout.getHeight();
                    setupViewPager(viewPager, height + actionBarHeight);
                    tabLayout.setupWithViewPager(viewPager);
                }
            });
        }


    }

    public void enable(boolean enable) {
        viewPager.setPagingEnabled(enable);
    }


    private void setupViewPager(CustomViewPager viewPager, int height) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setPagingEnabled(true);
        viewPagerAdapter.addFragment(WeatherFragment.newInstance(height), "Today");
//        viewPagerAdapter.addFragment(WeatherFragment.newInstance(height), "6 days");
        viewPagerAdapter.addFragment(new PiFragment(), "Real Time");
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_main_test;
    }

    @Override
    public void handleBus(Object event) {
        if (event instanceof MainEvent){
            viewPager.setPagingEnabled(((MainEvent) event).isPagerStatus());
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
