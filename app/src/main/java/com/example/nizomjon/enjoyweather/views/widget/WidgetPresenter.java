package com.example.nizomjon.enjoyweather.views.widget;

import android.content.Context;

import com.example.nizomjon.enjoyweather.api.RestService;
import com.example.nizomjon.enjoyweather.api.SkService;
import com.example.nizomjon.enjoyweather.api.callback.ApiCallback;
import com.example.nizomjon.enjoyweather.api.callback.SubscriberCallback;
import com.example.nizomjon.enjoyweather.api.exceptions.RetrofitException;
import com.example.nizomjon.enjoyweather.base.components.DaggerPresenterComponent;
import com.example.nizomjon.enjoyweather.base.module.NetModule;
import com.example.nizomjon.enjoyweather.base.module.UtilsModule;
import com.example.nizomjon.enjoyweather.model.CurrentWeatherSk;
import com.example.nizomjon.enjoyweather.model.SkWeather;
import com.example.nizomjon.enjoyweather.model.test.Forecast6days;
import com.example.nizomjon.enjoyweather.model.test.Response;
import com.example.nizomjon.enjoyweather.mvp.BasePresenter;
import com.example.nizomjon.enjoyweather.utils.AppUtils;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import rx.Scheduler;
import rx.functions.Func2;

/**
 * Created by Nizomjon on 16/03/2017.
 */

public class WidgetPresenter extends BasePresenter<WidgetView> {


    @Inject
    @Named("main_thread")
    Scheduler mMainThread;

    @Inject
    @Named("new_thread")
    Scheduler mNewThread;


    @Inject
    @Named(NetModule.PRODUCTION)
    RestService api;

    @Inject
    @Named(NetModule.SKPLANET)
    SkService skApi;


    @Inject
    AppUtils mUtils;

    public WidgetPresenter(Context context) {
        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(context))
                .build()
                .inject(this);
    }

    public void getWeatherData(double latitude, double longitude) {

        subscribe(Observable.zip(skApi.getCurrentWeatherCondition(longitude, latitude), skApi.getForecast6days(latitude, longitude)
                .map(Response::getWeather)
                .flatMap(weather -> Observable.from(weather.getForecast6days())
                ), new Func2<SkWeather, Forecast6days, CurrentWeatherSk>() {
            @Override
            public CurrentWeatherSk call(SkWeather skWeather, Forecast6days forecast) {
                return new CurrentWeatherSk(skWeather, forecast);
            }
        }), new SubscriberCallback<>(new ApiCallback<CurrentWeatherSk>() {
            @Override
            public void onSuccess(CurrentWeatherSk model) {
                getMvpView().showResult(model);
            }

            @Override
            public void onFailure(RetrofitException exception) {
                exception.getResponse();
            }

            @Override
            public void onCompleted() {

            }

            @Override
            public void onNetworkError() {
            }
        }));
    }

}
