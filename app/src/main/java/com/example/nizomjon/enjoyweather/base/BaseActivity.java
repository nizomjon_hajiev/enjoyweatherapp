package com.example.nizomjon.enjoyweather.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.example.nizomjon.enjoyweather.R;
import com.example.nizomjon.enjoyweather.base.components.DaggerActivityComponent;
import com.example.nizomjon.enjoyweather.base.module.NetModule;
import com.example.nizomjon.enjoyweather.base.module.PresenterModule;
import com.example.nizomjon.enjoyweather.base.module.UtilsModule;
import com.example.nizomjon.enjoyweather.utils.AppUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by Nizomjon on 09/02/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {
    @Inject
    public AppUtils appUtils;

    @Nullable
    @BindView(R.id.toolbar)
    public Toolbar toolbar;
    private Subscription busSubscription;

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);

        if (toolbar!=null){
            setSupportActionBar(toolbar);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(this))
                .netModule(new NetModule())
                .presenterModule(new PresenterModule(this))
                .build()
                .inject(this);
    }

    protected boolean interceptBackAction() {
        return false;
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    public void setHomeAsUp() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    public void onBackPressed() {
        if (!interceptBackAction()) {
            hideKeyboard();
            super.onBackPressed();
        }
    }
    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public abstract
    @LayoutRes
    int getLayout();

    @Override
    protected void onResume() {
        super.onResume();
        autoUnsubBus();
        busSubscription = WeatherApplication.get().bus().toObserverable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Action1<Object>() {
                            @Override
                            public void call(Object o) {
                                handleBus(o);
                            }
                        }
                );
    }

    private void autoUnsubBus() {
        if (busSubscription != null && !busSubscription.isUnsubscribed()) {
            busSubscription.unsubscribe();
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        autoUnsubBus();
    }

    public abstract void handleBus(Object event);
}
