package com.example.nizomjon.enjoyweather.api.response;

/**
 * Created by Nizomjon on 10/02/2017.
 */

public class LoginErrorResponse {
    private String error;
    private String status;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
