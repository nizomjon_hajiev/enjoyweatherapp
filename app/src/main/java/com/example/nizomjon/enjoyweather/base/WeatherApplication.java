package com.example.nizomjon.enjoyweather.base;

import android.app.Application;
import android.content.Context;

import com.example.nizomjon.enjoyweather.BuildConfig;
import com.example.nizomjon.enjoyweather.base.components.AppComponent;
import com.example.nizomjon.enjoyweather.base.components.DaggerAppComponent;
import com.example.nizomjon.enjoyweather.base.module.UtilsModule;
import com.example.nizomjon.enjoyweather.widgets.RxBus;
import com.facebook.stetho.DumperPluginsProvider;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.dumpapp.DumperPlugin;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by Nizomjon on 09/02/2017.
 */

public class WeatherApplication extends Application {

    private AppComponent mAppComponent;
    private static WeatherApplication instance;
    private RxBus bus;

    @Override
    public void onCreate() {
        super.onCreate();
        final Context context = this;

        mAppComponent = DaggerAppComponent.builder().utilsModule(new UtilsModule(this)).build();
        instance = this;
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        bus = new RxBus();

        Stetho.initialize(Stetho.newInitializerBuilder(context)
                .enableDumpapp(new SampleDumperPluginsProvider(context))
                .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(context)).build());
    }

    public static WeatherApplication get() {
        return instance;
    }


    public RxBus bus() {
        return bus;
    }

    private static class SampleDumperPluginsProvider implements DumperPluginsProvider {

        private final Context mContext;

        private SampleDumperPluginsProvider(Context mContext) {
            this.mContext = mContext;
        }

        @Override
        public Iterable<DumperPlugin> get() {
            ArrayList<DumperPlugin> plugins = new ArrayList<>();
            for (DumperPlugin defaultPlugin : Stetho.defaultDumperPluginsProvider(mContext).get()) {
                plugins.add(defaultPlugin);
            }
            return plugins;
        }
    }


}
