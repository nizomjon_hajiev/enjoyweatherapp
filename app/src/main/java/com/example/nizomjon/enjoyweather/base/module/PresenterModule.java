package com.example.nizomjon.enjoyweather.base.module;

import android.content.Context;

import com.example.nizomjon.enjoyweather.views.login.LoginPresenter;
import com.example.nizomjon.enjoyweather.views.main.MainPresenter;
import com.example.nizomjon.enjoyweather.views.main.fragments.fragmentPi.PiPresenter;
import com.example.nizomjon.enjoyweather.views.main.fragments.fragmentWeather.WeatherPresenter;
import com.example.nizomjon.enjoyweather.views.register.RegisterPresenter;
import com.example.nizomjon.enjoyweather.views.widget.WidgetPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Nizomjon on 09/02/2017.
 */
@Module
public class PresenterModule {
    private Context context;

    public PresenterModule(Context context) {
        this.context = context;
    }

    @Provides
    LoginPresenter loginPresenter() {
        return new LoginPresenter(context);
    }

    @Provides
    RegisterPresenter registerPresenter() {
        return new RegisterPresenter(context);
    }

    @Provides
    MainPresenter mainPresenter() {
        return new MainPresenter(context);
    }

    @Provides
    PiPresenter piPresenter() {
        return new PiPresenter(context);
    }

    @Provides
    WeatherPresenter weatherPresenter() {
        return new WeatherPresenter(context);
    }

    @Provides
    WidgetPresenter widgetPresenter() {
        return new WidgetPresenter(context);
    }
}
