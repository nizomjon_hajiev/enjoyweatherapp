package com.example.nizomjon.enjoyweather.base.components;

import com.example.nizomjon.enjoyweather.base.module.NetModule;
import com.example.nizomjon.enjoyweather.base.module.UtilsModule;
import com.example.nizomjon.enjoyweather.views.login.LoginPresenter;
import com.example.nizomjon.enjoyweather.views.main.MainPresenter;
import com.example.nizomjon.enjoyweather.views.main.fragments.fragmentPi.PiPresenter;
import com.example.nizomjon.enjoyweather.views.main.fragments.fragmentWeather.WeatherPresenter;
import com.example.nizomjon.enjoyweather.views.register.RegisterPresenter;
import com.example.nizomjon.enjoyweather.views.widget.WidgetPresenter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Nizomjon on 09/02/2017.
 */
@Singleton
@Component(modules = {NetModule.class, UtilsModule.class})
public interface PresenterComponent {
    void inject(LoginPresenter presenter);

    void inject(RegisterPresenter presenter);

    void inject(MainPresenter presenter);

    void inject(WeatherPresenter presenter);

    void inject(PiPresenter presenter);

    void inject(WidgetPresenter presenter);
}
