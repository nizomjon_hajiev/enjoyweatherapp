package com.example.nizomjon.enjoyweather.views.main;

import android.content.Context;
import android.widget.Toast;

import com.example.nizomjon.enjoyweather.api.RestService;
import com.example.nizomjon.enjoyweather.api.SkService;
import com.example.nizomjon.enjoyweather.api.callback.ApiCallback;
import com.example.nizomjon.enjoyweather.api.callback.SubscriberCallback;
import com.example.nizomjon.enjoyweather.api.exceptions.RetrofitException;
import com.example.nizomjon.enjoyweather.base.components.DaggerPresenterComponent;
import com.example.nizomjon.enjoyweather.base.module.NetModule;
import com.example.nizomjon.enjoyweather.base.module.UtilsModule;
import com.example.nizomjon.enjoyweather.model.SkWeather;
import com.example.nizomjon.enjoyweather.model.WeatherPi;
import com.example.nizomjon.enjoyweather.mvp.BasePresenter;
import com.example.nizomjon.enjoyweather.utils.AppUtils;
import com.google.gson.Gson;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Scheduler;

/**
 * Created by Nizomjon on 10/02/2017.
 */
public class MainPresenter extends BasePresenter<MainView> {

    @Inject
    @Named("main_thread")
    Scheduler mMainThread;

    @Inject
    @Named("new_thread")
    Scheduler mNewThread;

    @Inject
    @Named(NetModule.PRODUCTION)
    RestService api;

    @Inject
    @Named(NetModule.SKPLANET)
    SkService skApi;


    @Inject
    AppUtils mUtils;
    @Inject
    @Named(NetModule.PRODUCTION)
    Gson gson;
    private Context context;
    private MqttAndroidClient mqttAndroidClient;
    private MqttConnectOptions mqttConnectOptions;
    private String clientId = "UdblabClientId";
    private static final String main_broker_url = "ws://159.203.160.131:8080/mqtt";
    private static final String hive_broker_url = "tcp://broker.hivemq.com:1883";
    private static final String amazon_broker_url = "tcp://m12.cloudmqtt.com:13226";
    private static final String sub_current_data_topic = "udblab/sensor/+/current_data/";
    private static final String pub_senor_heating_topic = "udblab/sensor/1112/heating/";

    public MainPresenter(Context context) {

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(context))
                .build()
                .inject(this);
        this.context = context;
    }

    public void initMqtt(String broker_url, boolean isMain, boolean isHive) {

        clientId = clientId + System.currentTimeMillis();

        mqttAndroidClient = new MqttAndroidClient(context, broker_url, clientId);
        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {

                if (reconnect) {
                    getMvpView().onReConnected();
                    subscribeToTopic();
                } else {
                    getMvpView().onConnected("");
                }
            }


            @Override
            public void connectionLost(Throwable cause) {
                Toast.makeText(context, ("The Connection was lost."), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                String json = new String(message.getPayload());
                WeatherPi weatherPi = gson.fromJson(json, WeatherPi.class);
                getMvpView().onMessageArrived(weatherPi);
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });

        mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setCleanSession(!isMain);
        mqttConnectOptions.setUserName(isMain ? "udblab" : "hlqbtvzv");
        mqttConnectOptions.setPassword(isMain ? "12345".toCharArray() : "rGA8NiRaD2KX".toCharArray());
        mqttConnectOptions.setAutomaticReconnect(true);

        try {
            mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
                    disconnectedBufferOptions.setBufferEnabled(true);
                    disconnectedBufferOptions.setBufferSize(100);
                    disconnectedBufferOptions.setPersistBuffer(false);
                    disconnectedBufferOptions.setDeleteOldestMessages(false);
                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions);
                    subscribeToTopic();
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    if (isMain && isHive) {
                        initMqtt(amazon_broker_url, false, false);

                    } else if (!isMain && !isHive) {
                        initMqtt(hive_broker_url, true, false);
                    } else {
                        initMqtt(main_broker_url, true, true);
                    }
                    getMvpView().onFailed();
                }
            });


        } catch (MqttException ex) {
            ex.printStackTrace();
        }
    }

    private void subscribeToTopic() {
        try {
            mqttAndroidClient.subscribe(sub_current_data_topic, 0, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    getMvpView().onSubscribed();
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {

                }
            });
        } catch (MqttException ex) {
            System.err.println("Exception whilst subscribing");
            ex.printStackTrace();
        }
    }

    public void publishMessage(String publishMessage) {

        try {
            MqttMessage message = new MqttMessage();

            message.setPayload(publishMessage.getBytes());
            mqttAndroidClient.publish(pub_senor_heating_topic, message);
            if (!mqttAndroidClient.isConnected()) {
                Toast.makeText(context, "mqttAndroidClient.getBufferedMessageCount() + \" messages in buffer", Toast.LENGTH_SHORT).show();
            }
        } catch (MqttException e) {
            System.err.println("Error Publishing: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void onPauseMode() {
//        try {
//            if (mqttAndroidClient != null && mqttAndroidClient.isConnected()) {
//                mqttAndroidClient.unsubscribe(sub_current_data_topic);
//                mqttAndroidClient.setCallback(null);
//                mqttAndroidClient.disconnect();
//            }
//        } catch (MqttException e) {
//            e.printStackTrace();
//        }
    }

    void onRestartMode() {
//        if (mqttAndroidClient!=null && !mqttAndroidClient.isConnected()) {
//            initMqtt();
//        }
    }

    public void onDestroyMode() {
//        try {
//            if (mqttAndroidClient!=null && mqttAndroidClient.isConnected()){
//                mqttAndroidClient.unsubscribe(sub_current_data_topic);
//                mqttAndroidClient.disconnect();
//            }
//        } catch (MqttException e) {
//            e.printStackTrace();
//        }
    }

    public void syncWeatherData(double latitude, double longitude) {
        unSubscribeAll();
        subscribe(skApi.getCurrentWeatherCondition(latitude, longitude), new SubscriberCallback<>(new ApiCallback<SkWeather>() {
            @Override
            public void onSuccess(SkWeather model) {
                model.getResult();
            }


            @Override
            public void onFailure(RetrofitException exception) {
                exception.getResponse();
            }

            @Override
            public void onCompleted() {

            }

            @Override
            public void onNetworkError() {

            }
        }));
    }

}
