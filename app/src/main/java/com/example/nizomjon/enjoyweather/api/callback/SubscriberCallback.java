package com.example.nizomjon.enjoyweather.api.callback;

import com.example.nizomjon.enjoyweather.api.exceptions.RetrofitException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscriber;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 20.08.2016.
 */
public class SubscriberCallback<T> extends Subscriber<T> implements Callback<T> {

    private ApiCallback<T> mApiCallback;

    public SubscriberCallback(ApiCallback<T> apiCallback) {
        mApiCallback = apiCallback;
    }

    @Override
    public void onCompleted() {
        mApiCallback.onCompleted();
    }

    @Override
    public void onError(Throwable e) {
        RetrofitException error = (RetrofitException) e;
        if (error.getKind()== RetrofitException.Kind.NETWORK){
            mApiCallback.onNetworkError();
        }else {
            mApiCallback.onFailure(error);
        }
        mApiCallback.onCompleted();
    }

    @Override
    public void onNext(T t) {
        mApiCallback.onSuccess(t);
    }


    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        response.body();
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
    t.getMessage();
    }
}
