package com.example.nizomjon.enjoyweather.views.main.fragments.fragmentPi;

import com.example.nizomjon.enjoyweather.model.WeatherPi;
import com.example.nizomjon.enjoyweather.mvp.MvpView;

/**
 * Created by Nizomjon on 22/02/2017.
 */

public interface PiView extends MvpView{

    void onRestart();

    void onMessageArrived(WeatherPi weatherPi);

    void onConnected(String url);

    void onFailed();

    void onReConnected();

    void onSubscribed();
}
