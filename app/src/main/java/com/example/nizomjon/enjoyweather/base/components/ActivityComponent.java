package com.example.nizomjon.enjoyweather.base.components;

import com.example.nizomjon.enjoyweather.base.BaseActivity;
import com.example.nizomjon.enjoyweather.base.BaseFragment;
import com.example.nizomjon.enjoyweather.base.module.NetModule;
import com.example.nizomjon.enjoyweather.base.module.PresenterModule;
import com.example.nizomjon.enjoyweather.base.module.UtilsModule;
import com.example.nizomjon.enjoyweather.views.login.LoginActivity;
import com.example.nizomjon.enjoyweather.views.main.MainActivity;
import com.example.nizomjon.enjoyweather.views.main.fragments.fragmentPi.PiFragment;
import com.example.nizomjon.enjoyweather.views.main.fragments.fragmentWeather.WeatherFragment;
import com.example.nizomjon.enjoyweather.views.register.RegisterActivity;
import com.example.nizomjon.enjoyweather.views.widget.WidgetService;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Nizomjon on 09/02/2017.
 */
@Singleton
@Component(modules = {PresenterModule.class, UtilsModule.class, NetModule.class})
public interface ActivityComponent {

    void inject(BaseActivity baseActivity);

    void inject(LoginActivity loginActivity);

    void inject(RegisterActivity registerActivity);

//    void inject(MainActivity mainActivity);

    void inject(MainActivity mainActivity);


    void inject(BaseFragment baseFragment);

    void inject(PiFragment piFragment);

    void inject(WeatherFragment weatherFragment);
    void inject(WidgetService widgetService);
}
