package com.example.nizomjon.enjoyweather.utils;

import android.content.SharedPreferences;

import javax.inject.Singleton;

/**
 * Created by Nizomjon on 10/02/2017.
 */

@Singleton
public class Settings {

    private SharedPreferences preferences;

    public Settings(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    public void saveAuthToken(String token) {
        preferences.edit().putString("TOKEN", token).apply();
    }
    public String getAuthToken() {
        return preferences.getString("TOKEN", null);
    }

    public boolean isAuthorized() {
        return getAuthToken() != null;
    }

}
