package com.example.nizomjon.enjoyweather.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nizomjon.enjoyweather.base.components.DaggerActivityComponent;
import com.example.nizomjon.enjoyweather.base.module.NetModule;
import com.example.nizomjon.enjoyweather.base.module.PresenterModule;
import com.example.nizomjon.enjoyweather.base.module.UtilsModule;

import butterknife.ButterKnife;

/**
 * Created by Nizomjon on 22/02/2017.
 */

public abstract class BaseFragment extends Fragment {


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(getBaseActivity()))
                .netModule(new NetModule())
                .presenterModule(new PresenterModule(getBaseActivity()))
                .build()
                .inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(), container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @LayoutRes
    protected abstract int getLayout();


    @Override
    public void onDestroy() {
        super.onDestroy();
    }



    public void supportInvalidateOptionsMenu() {
        if (hasActivity()) {
            getActivity().supportInvalidateOptionsMenu();
        }
    }


    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    protected boolean hasActivity() {
        return getActivity() != null && !getActivity().isFinishing();
    }


    public void hideKeyboard() {
        if (hasActivity()) {
            getBaseActivity().hideKeyboard();
        }
    }
}
