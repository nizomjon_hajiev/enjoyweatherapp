package com.example.nizomjon.enjoyweather.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.example.nizomjon.enjoyweather.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


/**
 * Created by Nizomjon on 11/18/16.
 */

public class AppUtils {

    private Context mContext;

    public AppUtils(Context context) {
        this.mContext = context;
    }

    public boolean isOnline(View v) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        showSnackBar(v, mContext.getString(R.string.toast_network_not_available));
        return false;
    }

    public void hideSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showSnackBar(View view, String text) {
        Snackbar.make(view, text, Snackbar.LENGTH_SHORT).show();
    }

    public void showToast(String text) {
        Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();
    }

    //    public boolean checkPlayServices() {
//        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
//        int resultCode = apiAvailability.isGooglePlayServicesAvailable(mContext);
//        if (resultCode != ConnectionResult.SUCCESS) {
//            if (apiAvailability.isUserResolvableError(resultCode)) {
//                apiAvailability.getErrorDialog((Activity) mContext, resultCode, 9000)
//                        .show();
//            } else {
//                showToast(mContext.getResources().getString(R.string.warning_play_services));
//            }
//            return false;
//        }
//        return true;
//    }
    public void redirectToAppSettings() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", mContext.getPackageName(), null);
        intent.setData(uri);
        mContext.startActivity(intent);
    }

    public boolean isGpsEnabled() {
        final LocationManager manager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public void goToGpsSettings() {
        Intent callGPSSettingIntent = new Intent(
                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        mContext.startActivity(callGPSSettingIntent);
    }

    public String formatingDate(String format, Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    public String formatingDate(String format, String date) {
        try {
            DateFormat oldFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date newFormat = oldFormat.parse(date);
            DateFormat destDf = new SimpleDateFormat(format);
            date = destDf.format(newFormat);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public String weekOfName(Date date, int i) {
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(1, "Sunday");
        hashMap.put(2, "Monday");
        hashMap.put(3, "Tuesday");
        hashMap.put(4, "Wednesday");
        hashMap.put(5, "Thursday");
        hashMap.put(6, "Friday");
        hashMap.put(7, "Saturday");

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int position = calendar.get(Calendar.DAY_OF_WEEK); // 2
        int a;
        if (position + i <= 7) { // i = 6
            a = position + (i + 1);
        } else {
            a = position + (i + 1) - hashMap.size();
        }

        if (hashMap.containsKey(a)) {
            return hashMap.get(a);
        }
        return "Sunday";
    }

    // CurrentWeather
    public int getDayCode_currentWeather(String code) {
        String[] str = code.split("(^[_A-Z]*)");
        String b = str[1];
        HashMap<String, Integer> icons = new HashMap<>();
        icons.put("00", 38);
        icons.put("01", 1);
        icons.put("02", 2);
        icons.put("03", 3);
        icons.put("04", 12);
        icons.put("05", 13);
        icons.put("06", 14);
        icons.put("07", 18);
        icons.put("08", 21);
        icons.put("09", 32);
        icons.put("10", 4);
        icons.put("11", 29);
        icons.put("12", 26);
        icons.put("13", 27);
        icons.put("14", 28);
        if (icons.containsKey(b)) {
            return icons.get(b);
        }
        return 38;
    }

    public int getDayIcon_currentWeather(int code) {
        HashMap<Integer, Integer> icons = new HashMap<>();
        icons.put(1, R.drawable.icon_weather_1);
        icons.put(2, R.drawable.icon_weather_2);
        icons.put(3, R.drawable.icon_weather_3);
        icons.put(4, R.drawable.icon_weather_4);
        icons.put(12, R.drawable.icon_weather_12);
        icons.put(13, R.drawable.icon_weather_13);
        icons.put(14, R.drawable.icon_weather_14);
        icons.put(18, R.drawable.icon_weather_18);
        icons.put(21, R.drawable.icon_weather_21);
        icons.put(32, R.drawable.icon_weather_32);
        icons.put(29, R.drawable.icon_weather_29);
        icons.put(26, R.drawable.icon_weather_26);
        icons.put(27, R.drawable.icon_weather_27);
        icons.put(28, R.drawable.icon_weather_28);
        icons.put(38, R.drawable.icon_weather_38);
        if (icons.containsKey(code)) {
            return icons.get(code);
        }
        return R.drawable.icon_weather_1;
    }

    public int getNightCode_currentWeather(String code) {
        String[] str = code.split("(^[_A-Z]*)");
        String b = str[1];
        HashMap<String, Integer> icons = new HashMap<>();
        icons.put("01", 8);
        icons.put("02", 9);
        icons.put("03", 10);
        icons.put("04", 40);
        icons.put("05", 41);
        icons.put("06", 42);
        if (icons.containsKey(b)) {
            return icons.get(b);
        }
        return 38;
    }

    public int getNightIcon_currentWeather(int code) {
        HashMap<Integer, Integer> icons = new HashMap<>();
        icons.put(8, R.drawable.icon_weather_8);
        icons.put(9, R.drawable.icon_weather_9);
        icons.put(10, R.drawable.icon_weather_10);
        icons.put(40, R.drawable.icon_weather_40);
        icons.put(41, R.drawable.icon_weather_41);
        icons.put(42, R.drawable.icon_weather_42);
        if (icons.containsKey(code)) {
            return icons.get(code);
        }
        return R.drawable.icon_weather_1;
    }

}
