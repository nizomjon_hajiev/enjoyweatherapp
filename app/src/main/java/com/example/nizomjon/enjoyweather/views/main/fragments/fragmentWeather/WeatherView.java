package com.example.nizomjon.enjoyweather.views.main.fragments.fragmentWeather;

import com.example.nizomjon.enjoyweather.model.CurrentWeatherSk;
import com.example.nizomjon.enjoyweather.model.test.Response;
import com.example.nizomjon.enjoyweather.mvp.MvpView;

/**
 * Created by Nizomjon on 22/02/2017.
 */

public interface WeatherView extends MvpView {


    void fillData(CurrentWeatherSk currentWeatherSk);

    void fillGraph(Response response);

    void fillForecast10days(Response response);
}
