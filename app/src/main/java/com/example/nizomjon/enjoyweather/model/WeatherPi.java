package com.example.nizomjon.enjoyweather.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nizomjon on 10/02/2017.
 */

public class WeatherPi {

    @SerializedName("temp")
    @Expose
    private String temperature;
    @SerializedName("hum")
    @Expose
    private String humidity;
    @Expose
    @SerializedName("date")
    private String current_time;

    public String getTemperature() {
        return temperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public String getCurrent_time() {
        return current_time;
    }
}
