package com.example.nizomjon.enjoyweather.views.register;

import android.content.Context;

import com.example.nizomjon.enjoyweather.api.RestService;
import com.example.nizomjon.enjoyweather.api.SkService;
import com.example.nizomjon.enjoyweather.api.body.RegisterBody;
import com.example.nizomjon.enjoyweather.api.callback.ApiCallback;
import com.example.nizomjon.enjoyweather.api.callback.SubscriberCallback;
import com.example.nizomjon.enjoyweather.api.exceptions.RetrofitException;
import com.example.nizomjon.enjoyweather.api.response.LoginErrorResponse;
import com.example.nizomjon.enjoyweather.base.components.DaggerPresenterComponent;
import com.example.nizomjon.enjoyweather.base.module.NetModule;
import com.example.nizomjon.enjoyweather.base.module.UtilsModule;
import com.example.nizomjon.enjoyweather.model.User;
import com.example.nizomjon.enjoyweather.mvp.BasePresenter;
import com.example.nizomjon.enjoyweather.utils.AppUtils;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Scheduler;

/**
 * Created by Nizomjon on 10/02/2017.
 */

public class RegisterPresenter extends BasePresenter<RegisterView> {

    @Inject
    @Named("main_thread")
    Scheduler mMainThread;

    @Inject
    @Named("new_thread")
    Scheduler mNewThread;


    @Inject
    @Named(NetModule.PRODUCTION)
    RestService api;

    @Inject
    @Named(NetModule.SKPLANET)
    SkService skApi;


    @Inject
    AppUtils mUtils;

    public RegisterPresenter(Context context) {

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(context))
                .build()
                .inject(this);
    }

    public void register(String username, String email, String password) {
        getMvpView().showProgress();
        unSubscribeAll();
        RegisterBody body = new RegisterBody(username, email, password);
        subscribe(api.register(body), new SubscriberCallback<>(new ApiCallback<User>() {
            @Override
            public void onSuccess(User model) {
                getMvpView().onRegisterSuccess();
            }

            @Override
            public void onFailure(RetrofitException exception) {
                try {
                    LoginErrorResponse errorResponse = exception.getErrorBodyAs(LoginErrorResponse.class);
                    getMvpView().setError(errorResponse.getError());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }


            @Override
            public void onCompleted() {
                getMvpView().hideProgress();
                getMvpView().closeApplication();
            }

            @Override
            public void onNetworkError() {

            }
        }));
    }
}
