package com.example.nizomjon.enjoyweather.views.widget;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.widget.LinearLayout;
import android.widget.RemoteViews;

import com.example.nizomjon.enjoyweather.R;
import com.example.nizomjon.enjoyweather.api.SkService;
import com.example.nizomjon.enjoyweather.base.WeatherApplication;
import com.example.nizomjon.enjoyweather.base.components.DaggerActivityComponent;
import com.example.nizomjon.enjoyweather.base.module.NetModule;
import com.example.nizomjon.enjoyweather.base.module.PresenterModule;
import com.example.nizomjon.enjoyweather.base.module.UtilsModule;
import com.example.nizomjon.enjoyweather.model.CurrentWeatherSk;
import com.example.nizomjon.enjoyweather.utils.AppUtils;
import com.example.nizomjon.enjoyweather.views.main.MainActivity;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Nizomjon on 13/03/2017.
 */

public class WidgetService extends Service implements WidgetView {

    @Inject
    @Named(NetModule.SKPLANET)
    SkService skApi;

    @Inject
    AppUtils appUtils;

    @Inject
    WidgetPresenter presenter;

    LinearLayout widgetContainer;
    RemoteViews remoteViews;
    AppWidgetManager appWidgetManager;
    private Intent intent;
    NotificationManager notificationManager;
    @Override
    public void onCreate() {
        super.onCreate();
        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(WeatherApplication.get()))
                .netModule(new NetModule())
                .presenterModule(new PresenterModule(this))
                .build()
                .inject(this);
        presenter.attachView(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        updateWidget(intent);

        presenter.getWeatherData(36.9910, 127.9259); // TODO based on location
        this.intent = intent;
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        stopSelf();
        super.onStart(intent, startId);
    }

    private void updateWidget(Intent intent) {
        appWidgetManager = AppWidgetManager.getInstance(this
                .getApplicationContext());

        int[] allWidgetIds = intent
                .getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);
        for (int widgetId : allWidgetIds) {
            remoteViews = new RemoteViews(this
                    .getApplicationContext().getPackageName(),
                    R.layout.widget_layout_2_1);

            Intent clickIntent = new Intent(this.getApplicationContext(),
                    WidgetProvider.class);

            clickIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
            clickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,
                    allWidgetIds);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    getApplicationContext(), 0, clickIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.refreshBtn, pendingIntent);
//            remoteViews.removeAllViews(R.id.weeklyWeather);
            Intent openActivity = new Intent(this.getApplicationContext(), MainActivity.class);
            PendingIntent pendingActivity = PendingIntent.getActivity(this.getApplicationContext(), 0, openActivity, PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.widget_container, pendingActivity);
            appWidgetManager.updateAppWidget(widgetId, remoteViews);
        }
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void setError(String error) {

    }

    @Override
    public void showResult(CurrentWeatherSk response) {
        String temp = response.getSkWeather().getWeather().getHourlSk().get(0).getTemperatureSk().getTc();
        String tempMin = response.getSkWeather().getWeather().getHourlSk().get(0).getTemperatureSk().getTmin();
        String tempMax = response.getSkWeather().getWeather().getHourlSk().get(0).getTemperatureSk().getTmax();
        String updated_time = response.getSkWeather().getWeather().getHourlSk().get(0).getTimeObservation();
        String icon_code = response.getSkWeather().getWeather().getHourlSk().get(0).getSkySk().getCode();
        String icon_name = response.getSkWeather().getWeather().getHourlSk().get(0).getSkySk().getName();
        String precipation = String.valueOf(response.getSkWeather().getWeather().getHourlSk().get(0).getPrecipitation().getSinceOntime());
        String location = response.getSkWeather().getWeather().getHourlSk().get(0).getStation().getName();
        remoteViews.setImageViewResource(R.id.icon, appUtils.getDayIcon_currentWeather(appUtils.getDayCode_currentWeather(icon_code)));
        remoteViews.setTextViewText(R.id.precipationT, precipation);
        remoteViews.setTextViewText(R.id.updated_time, appUtils.formatingDate("hh:mm", updated_time));
        remoteViews.setTextViewText(R.id.temperature, String.valueOf(Math.ceil(Double.parseDouble(temp))));
        remoteViews.setTextViewText(R.id.dayAndNightTemp, "d:" + String.valueOf(Math.ceil(Double.parseDouble(tempMax))) + " n:" + String.valueOf(Math.ceil(Double.parseDouble(tempMin))));
        remoteViews.setTextViewText(R.id.location, location);
        showNotification(icon_code,location,icon_name,updated_time,temp);
        for (int i = 0; i < 3; i++) {
            RemoteViews view = new RemoteViews(this.getApplicationContext().getPackageName(), R.layout.widget_icon_container);
            String code = response.getForecast6days().getSkies().get(i).getSkyAm().getCode();
            view.setImageViewResource(R.id.icon, appUtils.getDayIcon_currentWeather(appUtils.getDayCode_currentWeather(code)));
            String tempAm = response.getForecast6days().getTemperatures().get(i).gettMax();
            String tempPm = response.getForecast6days().getTemperatures().get(i).gettMin();
            String weekOfDay = (appUtils.weekOfName(response.getForecast6days().getTimeRelease(), i));
            view.setTextViewText(R.id.day_of_week, weekOfDay);
            view.setTextViewText(R.id.temperature, tempAm + " " + tempPm);
            remoteViews.addView(R.id.weeklyWeather, view);
            response.getForecast6days().getTemperatures().get(i).gettMax();
            response.getForecast6days().getTemperatures().get(i).gettMin();

        }
        int[] allWidgetIds = intent
                .getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);
        for (int allWidgetId : allWidgetIds) {
            appWidgetManager.updateAppWidget(allWidgetId, remoteViews);
        }
    }

    // TODO move to the Notification Service
    private void showNotification(String icon_code,String location,String icon_name,String updated_time,String temp){

        RemoteViews remoteViews = new RemoteViews(this.getApplicationContext().getPackageName(), R.layout.notification_header_view);
        remoteViews.setImageViewResource(R.id.icon_notification, appUtils.getDayIcon_currentWeather(appUtils.getDayCode_currentWeather(icon_code)));
        remoteViews.setTextViewText(R.id.location, location);
        remoteViews.setTextViewText(R.id.sky_temperature_notification,icon_name + " " + temp);
        remoteViews.setTextViewText(R.id.updated_time_notification,updated_time);
        Intent resultIntent = new Intent(this, MainActivity.class);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        NotificationCompat.Builder mBuilder= new NotificationCompat.Builder(this);
        mBuilder.setContentIntent(resultPendingIntent);
        mBuilder.setSmallIcon(android.R.drawable.ic_menu_gallery);
        mBuilder.setAutoCancel(false);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        mBuilder.setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setContent(remoteViews);
        notificationManager.notify(1001,mBuilder.build());
    }
}
