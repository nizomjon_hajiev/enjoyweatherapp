package com.example.nizomjon.enjoyweather.views.register;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nizomjon.enjoyweather.R;
import com.example.nizomjon.enjoyweather.base.BaseActivity;
import com.example.nizomjon.enjoyweather.base.components.DaggerActivityComponent;
import com.example.nizomjon.enjoyweather.base.module.PresenterModule;
import com.example.nizomjon.enjoyweather.base.module.UtilsModule;
import com.example.nizomjon.enjoyweather.utils.DialogsUtil;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Nizomjon on 10/02/2017.
 */

public class RegisterActivity extends BaseActivity implements RegisterView {

    @Inject
    RegisterPresenter registerPresenter;

    @Inject
    DialogsUtil dialogsUtil;
    @BindView(R.id.username)
    EditText userNameT;
    @BindView(R.id.email)
    EditText emailT;
    @BindView(R.id.password)
    EditText passwordT;
    ProgressDialog progressDialog;
    private boolean isSuccess;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.loading));
        setHomeAsUp();
        setTitle("Register");

        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(this))
                .presenterModule(new PresenterModule(this))
                .build()
                .inject(this);
        registerPresenter.attachView(this);
    }

    @OnClick(R.id.registerBtn)
    void register() {
        String username = userNameT.getText().toString();
        String email = emailT.getText().toString();
        String password = passwordT.getText().toString();

        if (!username.isEmpty() && !password.isEmpty() && !email.isEmpty()) {
            registerPresenter.register(username, email, password);
        } else {
            Toast.makeText(this, "Please fill the fields", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_register;
    }

    @Override
    public void handleBus(Object event) {

    }

    @Override
    public void onRegisterSuccess() {
        isSuccess = true;
        Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void closeApplication() {
        if (isSuccess){
            Intent intent = new Intent();
            intent.putExtra("username", userNameT.getText().toString());
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public void showProgress() {
        progressDialog.show();

    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }


    @Override
    public void setError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}
