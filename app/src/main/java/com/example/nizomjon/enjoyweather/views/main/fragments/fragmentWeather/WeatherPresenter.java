package com.example.nizomjon.enjoyweather.views.main.fragments.fragmentWeather;

import android.content.Context;

import com.example.nizomjon.enjoyweather.api.SkService;
import com.example.nizomjon.enjoyweather.api.callback.ApiCallback;
import com.example.nizomjon.enjoyweather.api.callback.SubscriberCallback;
import com.example.nizomjon.enjoyweather.api.exceptions.RetrofitException;
import com.example.nizomjon.enjoyweather.base.components.DaggerPresenterComponent;
import com.example.nizomjon.enjoyweather.base.module.NetModule;
import com.example.nizomjon.enjoyweather.base.module.UtilsModule;
import com.example.nizomjon.enjoyweather.model.CurrentWeatherSk;
import com.example.nizomjon.enjoyweather.model.SkWeather;
import com.example.nizomjon.enjoyweather.model.test.Forecast;
import com.example.nizomjon.enjoyweather.model.test.Response;
import com.example.nizomjon.enjoyweather.mvp.BasePresenter;
import com.example.nizomjon.enjoyweather.utils.AppUtils;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import rx.Scheduler;
import rx.functions.Func2;

/**
 * Created by Nizomjon on 22/02/2017.
 */

public class WeatherPresenter extends BasePresenter<WeatherView> {


    @Inject
    @Named("main_thread")
    Scheduler mMainThread;

    @Inject
    @Named("new_thread")
    Scheduler mNewThread;

    @Inject
    AppUtils mUtils;


    @Inject
    @Named(NetModule.SKPLANET)
    SkService skApi;

    private Context context;

    private Gson gson;

    public WeatherPresenter(Context context) {
        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(context))
                .build()
                .inject(this);
        this.context = context;

    }


    void sync(double latitude, double longitude) {
//        unSubscribeAll();
        subscribe(Observable.zip(skApi.getCurrentWeatherCondition(longitude, latitude), skApi.sync(latitude, longitude)
                .map(Response::getWeather)
                .flatMap(weather -> Observable.from(weather.getForecast3hours())
                ), new Func2<SkWeather, Forecast, CurrentWeatherSk>() {
            @Override
            public CurrentWeatherSk call(SkWeather skWeather, Forecast forecast) {
                return new CurrentWeatherSk(skWeather, forecast);
            }
        }), new SubscriberCallback<>(new ApiCallback<CurrentWeatherSk>() {
            @Override
            public void onSuccess(CurrentWeatherSk model) {
                getMvpView().fillData(model);
            }

            @Override
            public void onFailure(RetrofitException exception) {
                exception.getResponse();
            }

            @Override
            public void onCompleted() {

            }

            @Override
            public void onNetworkError() {
            }
        }));


    }

    void forecast3hours(double latitude, double longitude) {
        subscribe(skApi.getForeCast3days(latitude, longitude), new SubscriberCallback<>(new ApiCallback<Response>() {
            @Override
            public void onSuccess(Response model) {
                getMvpView().fillGraph(model);
            }

            @Override
            public void onFailure(RetrofitException exception) {
                exception.getResponse();
            }

            @Override
            public void onCompleted() {

            }

            @Override
            public void onNetworkError() {

            }
        }));
    }

    void forecast6days(double latitude, double longitude) {
        subscribe(skApi.getForecast6days(latitude, longitude), new SubscriberCallback<>(new ApiCallback<Response>() {
            @Override
            public void onSuccess(Response model) {
                getMvpView().fillForecast10days(model);
            }

            @Override
            public void onFailure(RetrofitException exception) {
                exception.getResponse();
            }

            @Override
            public void onCompleted() {

            }

            @Override
            public void onNetworkError() {

            }
        }));
    }

}
