package com.example.nizomjon.enjoyweather.base.module;

import com.example.nizomjon.enjoyweather.api.RestService;
import com.example.nizomjon.enjoyweather.api.SkService;
import com.example.nizomjon.enjoyweather.api.exceptions.RxErrorHandlingCallAdapterFactory;
import com.example.nizomjon.enjoyweather.model.test.Forecast;
import com.example.nizomjon.enjoyweather.model.custom_deserializre.ForecastJsonDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Nizomjon on 09/02/2017.
 */
@Module
public class NetModule {

    private static final String PRODUCTION_API = "http://159.203.160.131/";
    private final static String BASE_URL_SK = "http://apis.skplanetx.com/weather/";
    public static final String BETA_API = "http://192.168.1.11:5000/";
    private static final String SK_API = "";
    public static final String PRODUCTION = "production";
    public static final String SKPLANET = "skplanet";


    @Singleton
    @Provides
    @Named(PRODUCTION)
    RestService provideService() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(PRODUCTION_API)
                .addConverterFactory(GsonConverterFactory.create(provideGson()))
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .client(provideClient())
                .build();
        return retrofit.create(RestService.class);
    }

    @Singleton
    @Provides
    @Named(SKPLANET)
    SkService provideServiceSk() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_SK)
                .addConverterFactory(GsonConverterFactory.create(provideGsonSk()))
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .client(provideClient())
                .build();
        return retrofit.create(SkService.class);
    }

    @Provides
    @Singleton
    @Named(PRODUCTION)
    Gson provideGson() {
        return new GsonBuilder()
                .setLenient()
                .create();
    }

    @Provides
    @Singleton
    @Named(SKPLANET)
    Gson provideGsonSk() {
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd hh:mm:ss")
                .registerTypeAdapter(Forecast.class, ForecastJsonDeserializer.getForecastJsonDeserializer())
                .create();
    }


    @Singleton
    @Provides
    OkHttpClient provideClient() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder()
                        .addHeader("Content-Type", "application/json;charset=utf-8")
                        .addHeader("Accept", "application/json")
                        .build();
                return chain.proceed(request);
            }
        });
        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(60, TimeUnit.SECONDS);
        return builder.build();
    }


}
