package com.example.nizomjon.enjoyweather.base.components;

import com.example.nizomjon.enjoyweather.base.module.UtilsModule;
import com.example.nizomjon.enjoyweather.utils.AppUtils;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Nizomjon on 09/02/2017.
 */
@Singleton
@Component(modules = {UtilsModule.class})
public interface AppComponent {
    void inject(AppUtils utils);
}
