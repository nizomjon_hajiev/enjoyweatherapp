package com.example.nizomjon.enjoyweather.api.callback;

import com.example.nizomjon.enjoyweather.api.exceptions.RetrofitException;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 27.08.2016.
 */
public interface ApiCallback<T> {

    void onSuccess(T model);

    void onFailure(RetrofitException exception);

    void onCompleted();

    void onNetworkError();

}
