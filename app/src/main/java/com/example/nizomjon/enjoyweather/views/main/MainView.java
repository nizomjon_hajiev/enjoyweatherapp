package com.example.nizomjon.enjoyweather.views.main;

import com.example.nizomjon.enjoyweather.model.WeatherPi;
import com.example.nizomjon.enjoyweather.mvp.MvpView;

/**
 * Created by Nizomjon on 10/02/2017.
 */

public interface MainView extends MvpView {

    void onMessageArrived(WeatherPi weatherPi);

    void onConnected(String url);

    void onFailed();

    void onReConnected();

    void onSubscribed();
}
