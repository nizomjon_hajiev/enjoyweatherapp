package com.example.nizomjon.enjoyweather.views.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nizomjon.enjoyweather.R;
import com.example.nizomjon.enjoyweather.api.response.UserResponse;
import com.example.nizomjon.enjoyweather.base.BaseActivity;
import com.example.nizomjon.enjoyweather.base.components.DaggerActivityComponent;
import com.example.nizomjon.enjoyweather.base.module.PresenterModule;
import com.example.nizomjon.enjoyweather.base.module.UtilsModule;
import com.example.nizomjon.enjoyweather.utils.DialogsUtil;
import com.example.nizomjon.enjoyweather.utils.Settings;
import com.example.nizomjon.enjoyweather.views.main.MainActivity;
import com.example.nizomjon.enjoyweather.views.register.RegisterActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Nizomjon on 09/02/2017.
 */

public class LoginActivity extends BaseActivity implements LoginView {

    public static final int REQUEST_CODE_REGISTER = 1001;

    @Inject
    LoginPresenter loginPresenter;
    @Inject
    Settings settings;

    @Inject
    DialogsUtil dialogsUtil;
    @BindView(R.id.username)
    EditText userNameT;
    @BindView(R.id.password)
    EditText passwordT;
    ProgressDialog progressDialog;
    private boolean isSuccess;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.loading));
        setTitle("Login");
        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(this))
                .presenterModule(new PresenterModule(this))
                .build()
                .inject(this);
        loginPresenter.attachView(this);
    }

    @OnClick(R.id.loginBtn)
    void login() {
        String username = userNameT.getText().toString();
        String password = passwordT.getText().toString();

        if (!username.isEmpty() && !password.isEmpty()) {
            loginPresenter.login(username, password);
        }
    }

    @OnClick(R.id.register_link)
    void register_link() {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivityForResult(intent, REQUEST_CODE_REGISTER);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_login;
    }

    @Override
    public void handleBus(Object event) {

    }

    @Override
    public void onLoginSuccess(UserResponse model) {
        isSuccess = true;
        settings.saveAuthToken(model.getUserName());
        Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void closeApplication() {
        if (isSuccess) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void showProgress() {
        progressDialog.show();

    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void setError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_REGISTER) {
            if (resultCode == RESULT_OK) {
                String username = data.getStringExtra("username");
                userNameT.setText(username);
            }
        }
    }
}
