package com.example.nizomjon.enjoyweather.api.body;

/**
 * Created by Nizomjon on 10/02/2017.
 */

public class LoginBody {
    private String username;
    private String password;

    public LoginBody(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
