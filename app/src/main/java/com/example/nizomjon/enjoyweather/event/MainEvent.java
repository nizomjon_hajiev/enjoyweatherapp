package com.example.nizomjon.enjoyweather.event;

/**
 * Created by Nizomjon on 27/02/2017.
 */

public class MainEvent {

    private Type type;
    private boolean pagerStatus;

    public enum Type {
        CONTROL_VIEWPAGER,
        UNCHECKED
    }

    public static MainEvent control_viewpager(boolean status) {
        MainEvent e = new MainEvent();
        e.pagerStatus = status;
        e.type = Type.CONTROL_VIEWPAGER;
        return e;
    }



    public Type getType() {
        return type;
    }

    public boolean isPagerStatus() {
        return pagerStatus;
    }
}
