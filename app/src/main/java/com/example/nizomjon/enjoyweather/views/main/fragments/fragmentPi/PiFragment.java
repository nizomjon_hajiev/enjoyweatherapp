package com.example.nizomjon.enjoyweather.views.main.fragments.fragmentPi;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nizomjon.enjoyweather.R;
import com.example.nizomjon.enjoyweather.base.BaseFragment;
import com.example.nizomjon.enjoyweather.base.components.DaggerActivityComponent;
import com.example.nizomjon.enjoyweather.base.module.PresenterModule;
import com.example.nizomjon.enjoyweather.base.module.UtilsModule;
import com.example.nizomjon.enjoyweather.model.WeatherPi;
import com.example.nizomjon.enjoyweather.utils.DialogsUtil;
import com.example.nizomjon.enjoyweather.utils.Settings;
import com.example.nizomjon.enjoyweather.widgets.CountDownAnimation;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;

/**
 * Created by Nizomjon on 22/02/2017.
 */

public class PiFragment extends BaseFragment implements PiView, CountDownAnimation.CountDownListener {


    @Inject
    Settings settings;
    @Inject
    DialogsUtil dialogsUtil;
    @Inject
    PiPresenter piPresenter;

    @BindView(R.id.tempValue)
    TextView tempT;
    @BindView(R.id.humValue)
    TextView humT;
    @BindView(R.id.dateValue)
    TextView dateT;
    @BindView(R.id.switchHeat)
    SwitchCompat switchIndicator;
    @BindView(R.id.heatingT)
    TextView heatingT;
    @BindView(R.id.counterT)
    TextView counterT;
    @BindView(R.id.webView)
    WebView webView;

    @BindView(R.id.ip_camera_btn)
    View ipCameraBtn;
    @Nullable
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private CountDownAnimation countDownAnimation;
    private static final String main_broker_url = "ws://159.203.160.131:8080/mqtt";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(getActivity()))
                .presenterModule(new PresenterModule(getActivity()))
                .build()
                .inject(this);
        piPresenter.attachView(this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        if (settings.isAuthorized()) {
            piPresenter.initMqtt(main_broker_url, true, true);
//        }
        initCountDownAnimation();
        initWebView();


        switchIndicator.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String heatingText = String.format("Turn %s heating", isChecked ? "off" : "on");
                heatingT.setText(heatingText);
                if (isChecked) {
                    piPresenter.publishMessage("android");
                    Animation scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f,
                            0.0f, Animation.RELATIVE_TO_SELF, 0.5f,
                            Animation.RELATIVE_TO_SELF, 0.5f);
                    countDownAnimation.setAnimation(scaleAnimation);
                    countDownAnimation.setStartCount(10);
                    countDownAnimation.start();
                }
            }
        });
    }

    private void initWebView() {
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {

            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                progressBar.setVisibility(View.GONE);
//                Toast.makeText(getActivity(), "Oh no! " + description, Toast.LENGTH_SHORT).show();
//                initWebView();

            }
        });
        webView.loadUrl("http://210.119.146.58/webcam");
    }

    private void initCountDownAnimation() {
        String heatingText = String.format("Turn %s heating", switchIndicator.isChecked() ? "on" : "off");
        heatingT.setText(heatingText);
        countDownAnimation = new CountDownAnimation(counterT, 10);
        countDownAnimation.setCountDownListener(this);
    }

    @OnClick(R.id.ip_camera_btn)
    void clickIpCam() {
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//        bottomSheetDialog.show();
    }

    @Optional
    @OnClick(R.id.close)
    void closeBottom() {
//        bottomSheetDialog.hide();
    }

    @Override
    public void onPause() {
        super.onPause();
        piPresenter.onPauseMode();
    }

//    @Override
//    public void onRestart() {
//        super.onRestart();
//        piPresenter.onRestartMode();
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        piPresenter.onDestroyMode();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void onRestart() {

    }

    @Override
    public void onMessageArrived(WeatherPi weatherPi) {
        tempT.setText(weatherPi.getTemperature());
        humT.setText(weatherPi.getHumidity());
        dateT.setText(weatherPi.getCurrent_time());
    }

    @Override
    public void onConnected(String main_broker_url) {
        if (getActivity() != null)
            Toast.makeText(getActivity(), "Connected  " + main_broker_url, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailed() {

    }

    @Override
    public void onReConnected() {

    }

    @Override
    public void onSubscribed() {
        if (getActivity() != null)
            Toast.makeText(getActivity(), "Subscribed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void setError(String error) {

    }


    @Override
    public void onCountDownEnd(CountDownAnimation animation) {
        switchIndicator.setChecked(false);
        String heatingText = String.format("Turn %s heating", switchIndicator.isChecked() ? "on" : "off");
        heatingT.setText(heatingText);
        Toast.makeText(getActivity(), "Done", Toast.LENGTH_SHORT).show();
    }
}
