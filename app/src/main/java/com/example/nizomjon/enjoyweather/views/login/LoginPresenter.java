package com.example.nizomjon.enjoyweather.views.login;

import android.content.Context;

import com.example.nizomjon.enjoyweather.api.RestService;
import com.example.nizomjon.enjoyweather.api.SkService;
import com.example.nizomjon.enjoyweather.api.body.LoginBody;
import com.example.nizomjon.enjoyweather.api.callback.ApiCallback;
import com.example.nizomjon.enjoyweather.api.callback.SubscriberCallback;
import com.example.nizomjon.enjoyweather.api.exceptions.RetrofitException;
import com.example.nizomjon.enjoyweather.api.response.LoginErrorResponse;
import com.example.nizomjon.enjoyweather.api.response.UserResponse;
import com.example.nizomjon.enjoyweather.base.components.DaggerPresenterComponent;
import com.example.nizomjon.enjoyweather.base.module.NetModule;
import com.example.nizomjon.enjoyweather.base.module.UtilsModule;
import com.example.nizomjon.enjoyweather.mvp.BasePresenter;
import com.example.nizomjon.enjoyweather.utils.AppUtils;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Scheduler;

/**
 * Created by Nizomjon on 09/02/2017.
 */

public class LoginPresenter extends BasePresenter<LoginView> {

    @Inject
    @Named("main_thread")
    Scheduler mMainThread;

    @Inject
    @Named("new_thread")
    Scheduler mNewThread;


    @Inject
    @Named(NetModule.PRODUCTION)
    RestService api;

    @Inject
    @Named(NetModule.SKPLANET)
    SkService skApi;


    @Inject
    AppUtils mUtils;

    public LoginPresenter(Context context) {

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(context))
                .build()
                .inject(this);
    }

    public void login(String username, String password) {
        getMvpView().showProgress();
        unSubscribeAll();
        LoginBody body = new LoginBody(username,password);

        subscribe(api.login(body), new SubscriberCallback<>(new ApiCallback<UserResponse>() {
            @Override
            public void onSuccess(UserResponse model) {
                if (model.getStatus().equals("success")){
                    getMvpView().onLoginSuccess(model);
                }
            }

            @Override
            public void onFailure(RetrofitException e) {
                try {
                    LoginErrorResponse errorResponse = e.getErrorBodyAs(LoginErrorResponse.class);
                    getMvpView().setError(errorResponse.getError());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }

            @Override
            public void onCompleted() {
                getMvpView().hideProgress();
                getMvpView().closeApplication();
            }

            @Override
            public void onNetworkError() {

            }
        }));
    }
}
