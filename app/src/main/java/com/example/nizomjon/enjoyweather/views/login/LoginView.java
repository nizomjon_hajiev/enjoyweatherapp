package com.example.nizomjon.enjoyweather.views.login;

import com.example.nizomjon.enjoyweather.api.response.UserResponse;
import com.example.nizomjon.enjoyweather.mvp.MvpView;

/**
 * Created by Nizomjon on 09/02/2017.
 */

public interface LoginView extends MvpView {

    void onLoginSuccess(UserResponse response);

    void closeApplication();
}
