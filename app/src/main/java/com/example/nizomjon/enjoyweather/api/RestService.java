package com.example.nizomjon.enjoyweather.api;


import com.example.nizomjon.enjoyweather.api.body.LoginBody;
import com.example.nizomjon.enjoyweather.api.body.RegisterBody;
import com.example.nizomjon.enjoyweather.api.response.UserResponse;
import com.example.nizomjon.enjoyweather.model.SkWeather;
import com.example.nizomjon.enjoyweather.model.User;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by Nizomjon on 09/02/2017.
 */
public interface RestService {

    @POST("/api/v1/register/")
    Observable<User> register(@Body RegisterBody registerBody);


    @POST("/api/v1/login/")
    Observable<UserResponse> login(@Body LoginBody loginBody);

    @GET
    @Headers({
            "Accept: application/json",
            "Content-Language: ko,ko-kr"
    })
    Observable<SkWeather> current_weather(@Url String url);
}
