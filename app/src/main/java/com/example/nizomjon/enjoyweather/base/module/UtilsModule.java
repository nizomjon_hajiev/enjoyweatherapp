package com.example.nizomjon.enjoyweather.base.module;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.nizomjon.enjoyweather.utils.AppUtils;
import com.example.nizomjon.enjoyweather.utils.DialogsUtil;
import com.example.nizomjon.enjoyweather.utils.Settings;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Nizomjon on 09/02/2017.
 */
@Module
public class  UtilsModule {

    private Context context;

    public UtilsModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    AppUtils getAppUtils() {
        return new AppUtils(context);
    }

    @Provides
    @Singleton
    DialogsUtil getDialogUtils() {
        return new DialogsUtil(context);
    }

    @Provides
    @Singleton
    @Named("new_thread")
    Scheduler getNewThread() {
        return Schedulers.io();
    }

    @Provides
    @Singleton
    @Named("main_thread")
    Scheduler getMainThread() {
        return AndroidSchedulers.mainThread();
    }


    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Provides
    @Singleton
    Settings provideSettings() {
        return new Settings(provideSharedPreferences());
    }
}
