package com.example.nizomjon.enjoyweather.mvp;

/**
 * Created by Nizomjon on 11/18/16.
 */

public interface MvpView {


    void showProgress();

    void hideProgress();

    void setError(String error);

}
