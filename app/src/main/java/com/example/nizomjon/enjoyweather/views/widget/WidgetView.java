package com.example.nizomjon.enjoyweather.views.widget;

import com.example.nizomjon.enjoyweather.model.CurrentWeatherSk;
import com.example.nizomjon.enjoyweather.mvp.MvpView;

/**
 * Created by Nizomjon on 16/03/2017.
 */

public interface WidgetView extends MvpView {


    void showResult(CurrentWeatherSk response);

}
